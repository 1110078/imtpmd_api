<?php

  Route::get('gifts', 'Controller@giftsController');
  Route::get('/gifts/{gifts}', 'Controller@giftsId');

  Auth::routes();
