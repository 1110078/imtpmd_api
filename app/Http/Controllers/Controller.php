<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\GiftsTable;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function giftsController() {
      $gifts = DB::table('gifts')->get()->toJson();
      $giftsView = view('gifts', ['gifts' => $gifts]);
      return $giftsView;
    }
    //For a singular gift
    public function giftsId($gifts) {
      $chosenGift = GiftsTable::where('id','=',$gifts)->get()->first()->toJson();
      return view('id', ['gifts' => $chosenGift]);
    }
}
