<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BackController extends Controller
{
    public function backHome() {
      return view("/")
    }

    public function backUserHome() {
      return view("/user")
    }

}
