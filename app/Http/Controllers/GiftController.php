<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController as BaseController;
use App\GiftsTable;
use Validator;

class GiftController extends BaseController {
    public function index() {
        $gifts = GiftsTable::all();
        return $this->sendResponse($gifts->toArray(), 'Gifts retrieved successfully.');
    }

    public function show($id) {
        $gift = GiftsTable::find($id);

        if (is_null($gift)) {
            return $this->sendError('Gift not found.');
        }
        return $this->sendResponse($gift->toArray(), 'Product retrieved successfully.');
    }
}
