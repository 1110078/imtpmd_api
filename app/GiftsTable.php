<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GiftsTable extends Model
{
    protected $table = "gifts";
}
