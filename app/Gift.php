<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'category', 'price', 'interest', 'holiday', 'age', 'sex', 'image'
    ];
}
